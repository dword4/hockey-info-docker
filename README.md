# hockey-info-docker

Dockerfile to deploy the hockey-info app quickly and easily

## How to use this Docker image

```sh
$ docker build -t hockey-info . && docker run -d -p 80:5000 hockey-info
```
